package jp.masakura.utility

import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertSame

class ObjectProviderTest {
    private lateinit var target: ObjectProvider<String, Object>

    @Before
    fun setUp() {
        target = ObjectProvider<String, Object> { Object(it) }
    }

    @Test
    fun testGetCreate() {
        assertEquals(Object("abc"), target.get("abc"))
    }

    @Test
    fun testGetCached() {
        val first = target.get("abc")
        val second = target.get("abc")

        assertSame(first, second)
    }

    @Test
    fun testGetDifferent() {
        val first = target.get("abc")
        val second = target.get("def")

        assertNotEquals(first, second)
    }
}

data class Object(private val text: String)
