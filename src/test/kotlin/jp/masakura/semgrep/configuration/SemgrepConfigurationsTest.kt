package jp.masakura.semgrep.configuration

import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertTrue

class SemgrepConfigurationsTest {
    @Test
    fun testAppend() {
        val target = SemgrepConfigurations().append("r/java").append("r/csharp")

        assertEquals("r/java,r/csharp", target.toString())
    }

    @Test
    fun testParse() {
        val target = SemgrepConfigurations.parse("r/java,r/csharp")

        assertEquals(listOf("r/java", "r/csharp"), target.configurations)
    }

    @Test
    fun testParseWithWhitespace() {
        val target = SemgrepConfigurations.parse("r/java, r/csharp")

        assertEquals(listOf("r/java", "r/csharp"), target.configurations)
    }

    @Test
    fun testIsEmptyTrue() {
        val target = SemgrepConfigurations.parse(null)

        assertTrue(target.isEmpty())
    }

    @Test
    fun testIsEmptyTrueEmptyString() {
        val target = SemgrepConfigurations.parse("   ")

        assertTrue(target.isEmpty())
    }

    @Test
    fun testIsEmptyFalse() {
        val target = SemgrepConfigurations.parse("r/java")

        assertFalse(target.isEmpty())
    }

    @Test
    fun testHashCodeEquals() {
        val left = SemgrepConfigurations.parse("auto")
        val right = SemgrepConfigurations.parse("auto")

        assertEquals(left.hashCode(), right.hashCode())
    }

    @Test
    fun testHashCodeNotEquals() {
        val left = SemgrepConfigurations.parse("auto")
        val right = SemgrepConfigurations.parse("r/java")

        assertNotEquals(left.hashCode(), right.hashCode())
    }

    @Test
    fun testEquals() {
        val left = SemgrepConfigurations.parse("auto")
        val right = SemgrepConfigurations.parse("auto")

        assertTrue(left == right)
    }

    @Test
    fun testNotEquals() {
        val left = SemgrepConfigurations.parse("auto")
        val right = SemgrepConfigurations.parse("r/java")

        assertTrue(left != right)
    }
}
