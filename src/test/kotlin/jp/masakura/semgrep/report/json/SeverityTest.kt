package jp.masakura.semgrep.report.json

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertArrayEquals

class SeverityTest {
    @Test
    fun testSeverity() {
        val json = """
            ["Error","Warning","Info"]
        """

        val actual = jacksonObjectMapper().readValue<List<Severity>>(json).toTypedArray()

        assertArrayEquals(arrayOf(Severity.Error, Severity.Warning, Severity.Info), actual)
    }
}
