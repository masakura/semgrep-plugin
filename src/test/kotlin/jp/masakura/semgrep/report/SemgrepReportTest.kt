package jp.masakura.semgrep.report

import jp.masakura.semgrep.report.json.Point
import jp.masakura.semgrep.report.json.Root
import jp.masakura.semgrep.report.json.Severity
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals

class SemgrepReportTest {
    @Test
    fun testResultSize() {
        assertEquals(1, deserialize().results.size)
    }

    @Test
    fun testResultPath() {
        assertEquals("src/TestExecutor.java", deserialize().results[0].path)
    }

    @Test
    fun testResultStart() {
        assertEquals(Point(3, 9, 67), deserialize().results[0].start)
    }

    @Test
    fun testResultEnd() {
        assertEquals(Point(3, 36, 94), deserialize().results[0].end)
    }

    @Test
    fun testResultSeverity() {
        assertEquals(Severity.Error, deserialize().results[0].extra.severity)
    }

    @Test
    fun testResultMessage() {
        assertEquals(
            "A formatted or concatenated string was detected as input to a ProcessBuilder call. This is dangerous if a variable is controlled by user input and could result in a command injection. Ensure your variables are not controlled by users or sufficiently sanitized.",
            deserialize().results[0].extra.message,
        )
    }

    companion object {
        private fun deserialize(): Root {
            return SemgrepReport.from(json.byteInputStream(Charsets.UTF_8)).root
        }

        private val json = """
            {
              "errors": [],
              "paths": {
                "_comment": "<add --verbose for a list of skipped paths>",
                "scanned": [
                  "src/TestExecutor.java"
                ]
              },
              "results": [
                {
                  "check_id": "java.lang.security.audit.command-injection-process-builder.command-injection-process-builder",
                  "end": {
                    "col": 36,
                    "line": 3,
                    "offset": 94
                  },
                  "extra": {
                    "fingerprint": "d351e334f29ae9e9c4322c05fed44a364bfcf2858861e419315cdf15d088655f6a34e99440df65bcba36b053b511cf1ff2eedcfe34e74c5e0743fcba8d3210b2_0",
                    "is_ignored": false,
                    "lines": "        new ProcessBuilder(command);",
                    "message": "A formatted or concatenated string was detected as input to a ProcessBuilder call. This is dangerous if a variable is controlled by user input and could result in a command injection. Ensure your variables are not controlled by users or sufficiently sanitized.",
                    "metadata": {
                      "category": "security",
                      "confidence": "LOW",
                      "cwe": [
                        "CWE-78: Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection')"
                      ],
                      "cwe2021-top25": true,
                      "cwe2022-top25": true,
                      "impact": "HIGH",
                      "license": "Commons Clause License Condition v1.0[LGPL-2.1-only]",
                      "likelihood": "LOW",
                      "owasp": [
                        "A01:2017 - Injection",
                        "A03:2021 - Injection"
                      ],
                      "references": [
                        "https://owasp.org/Top10/A03_2021-Injection"
                      ],
                      "shortlink": "https://sg.run/gJJe",
                      "source": "https://semgrep.dev/r/java.lang.security.audit.command-injection-process-builder.command-injection-process-builder",
                      "subcategory": [
                        "audit"
                      ],
                      "technology": [
                        "java"
                      ]
                    },
                    "metavars": {
                      "${'$'}CMD": {
                        "abstract_content": "command",
                        "end": {
                          "col": 35,
                          "line": 3,
                          "offset": 93
                        },
                        "start": {
                          "col": 28,
                          "line": 3,
                          "offset": 86
                        }
                      }
                    },
                    "severity": "ERROR"
                  },
                  "path": "src/TestExecutor.java",
                  "start": {
                    "col": 9,
                    "line": 3,
                    "offset": 67
                  }
                }
              ],
              "version": "1.2.1"
            }
        """
    }
}
