package jp.masakura.semgrep.jetbrains.annotation

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.TextRange
import jp.masakura.semgrep.report.json.Extra
import jp.masakura.semgrep.report.json.Point
import jp.masakura.semgrep.report.json.Result
import jp.masakura.semgrep.report.json.Severity
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals

class SemgrepAnnotationTest {
    @Test
    fun testFromSemgrepResult() {
        val actual = SemgrepAnnotation.from(
            Result(
                "foo/Bar.java",
                Point(1, 5, 34),
                Point(1, 20, 40),
                Extra(Severity.Error, "code problem"),
            ),
        )

        assertEquals(
            SemgrepAnnotation(
                HighlightSeverity.ERROR,
                TextRange(34, 40),
                "Semgrep: code problem",
            ),
            actual,
        )
    }

    @Test
    fun testErrorSeverity() {
        val actual = convertSeverity(Severity.Error)

        assertEquals(actual, HighlightSeverity.ERROR)
    }

    @Test
    fun testWarningSeverity() {
        val actual = convertSeverity(Severity.Warning)

        assertEquals(actual, HighlightSeverity.WARNING)
    }

    @Test
    fun testInformationSeverity() {
        val actual = convertSeverity(Severity.Info)

        assertEquals(actual, HighlightSeverity.INFORMATION)
    }

    private fun convertSeverity(semgrepSeverity: Severity) = SemgrepAnnotation.from(
        Result(
            "foo/Bar.java",
            Point(1, 5, 34),
            Point(1, 20, 40),
            Extra(semgrepSeverity, "code problem"),
        ),
    ).severity
}
