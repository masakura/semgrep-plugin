package jp.masakura.semgrep.jetbrains.runner

import jp.masakura.semgrep.jetbrains.file.TemporaryFile
import jp.masakura.semgrep.jetbrains.file.TemporaryFileFactory
import jp.masakura.semgrep.report.SemgrepReport
import jp.masakura.semgrep.report.json.Root
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when` as mockitoWhen

class ModifyingFileSemgrepRunnerTest {
    private lateinit var report: SemgrepReport
    private lateinit var mockTemporaryFileFactory: TemporaryFileFactory
    private lateinit var mockRunner: SemgrepRunner
    private lateinit var target: ModifyingFileSemgrepRunner

    @Before
    fun setUp() {
        report = SemgrepReport(Root("1.0", emptyList()))
        mockRunner = mock(SemgrepRunner::class.java)
        mockTemporaryFileFactory = mock(TemporaryFileFactory::class.java)
        target = ModifyingFileSemgrepRunner(mockRunner, mockTemporaryFileFactory)
    }

    @Test
    fun testRunWithExtension() {
        val temporaryFile = MockFile("temporary.js")
        mockitoWhen(mockTemporaryFileFactory.create(".js")).thenReturn(temporaryFile)
        mockitoWhen(mockRunner.run(TargetFile("temporary.js", "js", "content"))).thenReturn(report)

        val actual = target.run(TargetFile("temporary.js", "js", "content"))

        assertEquals(report, actual)
        assertEquals("content", temporaryFile.content)
        assertTrue(temporaryFile.deleted)
    }

    @Test
    fun testRunWithoutExtension() {
        val temporaryFile = MockFile("temporary")
        mockitoWhen(mockTemporaryFileFactory.create(null)).thenReturn(temporaryFile)
        mockitoWhen(mockRunner.run(TargetFile("temporary", "", "content"))).thenReturn(report)

        val actual = target.run(TargetFile("temporary", "", "content"))

        assertEquals(report, actual)
    }

    @Test
    fun testRunNullExtension() {
        val temporaryFile = MockFile("temporary")
        mockitoWhen(mockTemporaryFileFactory.create(null)).thenReturn(temporaryFile)
        mockitoWhen(mockRunner.run(TargetFile("temporary", null, "content"))).thenReturn(report)

        val actual = target.run(TargetFile("temporary", null, "content"))

        assertEquals(report, actual)
    }

    private class MockFile(override val path: String) : TemporaryFile {
        private var _content = ""
        private var _deleted = false

        val content: String
            get() = _content

        val deleted: Boolean
            get() = _deleted

        override fun save(content: String): TemporaryFile {
            this._content = content
            return this
        }

        override fun delete() {
            _deleted = true
        }
    }
}
