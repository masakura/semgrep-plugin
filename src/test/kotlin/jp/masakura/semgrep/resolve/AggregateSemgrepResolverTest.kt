package jp.masakura.semgrep.resolve

import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull

class AggregateSemgrepResolverTest {
    @Test
    fun testResolveFirst() {
        val target = AggregateSemgrepResolver(
            listOf(
                FakeResolver("first"),
                FakeResolver("second"),
            ),
        )

        val actual = target.resolve()

        assertEquals("first", actual)
    }

    @Test
    fun testResolveIncludeNull() {
        val target = AggregateSemgrepResolver(
            listOf(
                FakeResolver(null),
                FakeResolver("first"),
                FakeResolver("second"),
            ),
        )

        val actual = target.resolve()

        assertEquals("first", actual)
    }

    @Test
    fun testResolveAllNull() {
        val target = AggregateSemgrepResolver(
            listOf(
                FakeResolver(null),
                FakeResolver(null),
                FakeResolver(null),
            ),
        )

        val actual = target.resolve()

        assertNull(actual)
    }
}

class FakeResolver(private val path: String?) : SemgrepResolver {
    override fun resolve(): String? {
        return path
    }
}
