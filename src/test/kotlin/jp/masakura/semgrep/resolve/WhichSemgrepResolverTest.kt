package jp.masakura.semgrep.resolve

import jp.masakura.command.Command
import jp.masakura.command.CommandResult
import jp.masakura.command.StandardInput
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull

class WhichSemgrepResolverTest {
    @Test
    fun testResolve() {
        val command = FakeWhich.found("semgrep", "/home/user/.local/bin/semgrep")

        val actual = WhichSemgrepResolver(command).resolve()

        assertEquals("/home/user/.local/bin/semgrep", actual)
    }

    @Test
    fun testResolveNotFound() {
        val command = FakeWhich.notFound("semgrep")

        val actual = WhichSemgrepResolver(command).resolve()

        assertNull(actual)
    }
}

class FakeWhich(private val command: String, private val standardOutput: String) : Command {
    override fun run(parameters: List<String>): CommandResult {
        if (parameters != listOf(command)) throw IllegalStateException()

        return CommandResult(StandardInput(standardOutput.byteInputStream()))
    }

    companion object {
        fun found(command: String, standardOutput: String): FakeWhich {
            return FakeWhich(command, standardOutput)
        }

        fun notFound(command: String): Command {
            return FakeWhich(command, "")
        }
    }
}
