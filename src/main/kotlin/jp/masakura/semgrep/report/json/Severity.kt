package jp.masakura.semgrep.report.json

enum class Severity {
    Info,
    Warning,
    Error,
}
