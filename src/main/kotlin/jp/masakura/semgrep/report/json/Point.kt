package jp.masakura.semgrep.report.json

data class Point(val line: Int, val col: Int, val offset: Int)
