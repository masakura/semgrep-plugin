package jp.masakura.semgrep.report.json

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Result(val path: String, val start: Point, val end: Point, val extra: Extra)
