package jp.masakura.semgrep.report

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import jp.masakura.semgrep.report.json.Root
import java.io.InputStream

class SemgrepReport(val root: Root) {

    companion object {
        val empty: SemgrepReport = SemgrepReport(Root.empty)

        fun from(json: InputStream): SemgrepReport {
            val serializer = jacksonObjectMapper()
            val root = serializer.readValue<Root>(json)
            return SemgrepReport(root)
        }
    }
}
