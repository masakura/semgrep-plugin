package jp.masakura.semgrep.configuration

class SemgrepConfigurations {
    val configurations: List<String>

    fun isEmpty(): Boolean = configurations.isEmpty()

    fun append(configuration: String): SemgrepConfigurations {
        return SemgrepConfigurations((configurations + listOf(configuration)))
    }

    override fun toString(): String {
        return configurations.joinToString(",")
    }

    constructor() : this(listOf())

    private constructor(strings: List<String>) {
        this.configurations = strings
    }

    fun values(): List<String> {
        if (configurations.isEmpty()) return listOf("auto")
        return configurations
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SemgrepConfigurations

        if (configurations != other.configurations) return false

        return true
    }

    override fun hashCode(): Int {
        return configurations.hashCode()
    }

    companion object {
        fun parse(s: String?): SemgrepConfigurations {
            if (s == null || s.trim() == "") return SemgrepConfigurations()
            return SemgrepConfigurations(s.split(",").map { it.trim() })
        }
    }
}
