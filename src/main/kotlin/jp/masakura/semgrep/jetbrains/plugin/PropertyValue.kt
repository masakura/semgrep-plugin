package jp.masakura.semgrep.jetbrains.plugin

import com.intellij.ide.util.PropertiesComponent
import org.jetbrains.annotations.NonNls

class PropertyValue(private val name: String, private val properties: PropertiesComponent = PropertiesComponent.getInstance()) {
    fun get(): @NonNls String? = properties.getValue(name)
    fun set(value: String?) {
        properties.setValue(name, value)
    }
    fun unset() {
        properties.unsetValue(name)
    }
}
