package jp.masakura.semgrep.jetbrains.plugin

import com.intellij.openapi.project.Project
import jp.masakura.semgrep.configuration.SemgrepConfigurations
import jp.masakura.semgrep.resolve.SemgrepResolver
import jp.masakura.semgrep.scanning.SemgrepScanner

class PropertiesSemgrepSettings(
    private val semgrepResolver: SemgrepResolver,
    private val project: Project,
) : SemgrepSettings {
    fun scanner(): SemgrepScanner? {
        if (executablePath == null) return null

        return SemgrepScanner(executablePath!!, project.basePath!!, configurations)
    }

    private val properties = SettingsProperties.project(project)
    private val configurationsValue = properties.property("jp.masakura.semgrep.configurations")

    override var configurations: SemgrepConfigurations
        get() = SemgrepConfigurations.parse(configurationsValue.get())
        set(value) {
            if (value.isEmpty()) {
                configurationsValue.unset()
                return
            }
            configurationsValue.set(value.toString())
        }

    override val executablePath: String?
        get() = semgrepResolver.resolve()

    companion object {
        fun project(project: Project): PropertiesSemgrepSettings {
            return PropertiesSemgrepSettings(
                SemgrepResolver.project(project),
                project,
            )
        }
    }
}
