package jp.masakura.semgrep.jetbrains.runner

import jp.masakura.semgrep.jetbrains.file.TemporaryFileFactory
import jp.masakura.semgrep.report.SemgrepReport

class ModifyingFileSemgrepRunner(
    private val runner: SemgrepRunner,
    private val temporaryFileFactory: TemporaryFileFactory = TemporaryFileFactory.default,
) : SemgrepRunner {
    override fun run(file: TargetFile): SemgrepReport {
        file.createTemporaryFile(temporaryFileFactory).use {
            return runner.run(it.file)
        }
    }
}
