package jp.masakura.semgrep.jetbrains.runner

import com.intellij.openapi.project.Project
import jp.masakura.semgrep.jetbrains.plugin.PropertiesSemgrepSettings
import jp.masakura.semgrep.report.SemgrepReport

interface SemgrepRunner {
    fun run(file: TargetFile): SemgrepReport

    companion object {

        fun project(project: Project): SemgrepRunner {
            val settings = PropertiesSemgrepSettings.project(project)
            val scanner = settings.scanner() ?: return NullSemgrepRunner.default

            val runner = SemgrepRunnerImpl(scanner)
            return ModifyingFileSemgrepRunner(runner)
        }
    }
}
