package jp.masakura.semgrep.jetbrains.file

import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.util.io.FileUtilRt
import java.io.File

class TemporaryFileImpl(private val file: File) : TemporaryFile {
    companion object {
        private val logger = Logger.getInstance(TemporaryFileImpl::class.java)

        fun create(extension: String?): TemporaryFileImpl {
            val file = FileUtilRt.createTempFile("semgrep-target", extension)
            logger.debug("create semgrep temporary target file. (${file.path})")
            return TemporaryFileImpl(file)
        }
    }

    override val path: String
        get() = file.path

    override fun save(content: String): TemporaryFile {
        file.writeText(content)
        return this
    }

    override fun delete() {
        file.delete()
    }
}
