package jp.masakura.semgrep.jetbrains.file

class TemporaryFileFactoryImpl : TemporaryFileFactory {
    override fun create(extension: String?): TemporaryFile {
        return TemporaryFileImpl.create(extension)
    }
}
