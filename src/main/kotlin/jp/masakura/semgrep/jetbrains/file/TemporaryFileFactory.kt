package jp.masakura.semgrep.jetbrains.file

interface TemporaryFileFactory {
    fun create(extension: String?): TemporaryFile

    companion object {
        val default = TemporaryFileFactoryImpl()
    }
}
