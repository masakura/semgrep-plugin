package jp.masakura.file

interface FileSystem {
    fun get(path: String): File

    companion object {
        val default: FileSystem = FileSystemImpl()
    }
}
