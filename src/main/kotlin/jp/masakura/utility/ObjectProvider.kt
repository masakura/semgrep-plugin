package jp.masakura.utility

class ObjectProvider<TKey, TValue>(private val create: (key: TKey) -> TValue) {
    private val map = HashMap<TKey, TValue>()

    fun get(key: TKey): TValue {
        var value = map[key]
        if (value == null) {
            value = create(key)
            map[key] = value
        }
        return value!!
    }
}
