package jp.masakura.command

import com.intellij.execution.configurations.GeneralCommandLine

class CommandFactory(private val aggregation: WithAggregation = WithAggregation()) {
    fun with(with: (GeneralCommandLine) -> GeneralCommandLine): CommandFactory {
        return CommandFactory(aggregation.add(with))
    }

    fun command(): Command {
        return CommandImpl(aggregation)
    }
}
