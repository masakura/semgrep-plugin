package jp.masakura.command

import com.intellij.openapi.diagnostic.Logger

class CommandImpl(private val aggregation: WithAggregation) : Command {
    override fun run(parameters: List<String>): CommandResult {
        val commandLine = aggregation.add { c -> c.withParameters(parameters) }.commandLine()

        logger.debug(commandLine.commandLineString)

        val process = commandLine
            .createProcess()

        return CommandResult.from(process)
    }

    companion object {
        private val logger = Logger.getInstance(CommandImpl::class.java)
    }
}
